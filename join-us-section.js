const SectionCreator = {
    create(type) {
      if (type === 'standard') {
        return new StandardProgram();
      } else if (type === 'advanced') {
        return new AdvancedProgram();
      } else {
        throw new Error('Invalid program type.');
      }
    }
  };

  function StandardProgram() {
    this.section = document.createElement('div');
    this.section.id = 'joinProgram';
    this.section.classList.add('standard-program');
  
    const heading = document.createElement('h2');
    heading.textContent = 'Join Our Program';
  
    const description = document.createElement('p');
    description.textContent = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua';
  
    const form = document.createElement('form');
    form.addEventListener('submit', this.handleFormSubmit.bind(this));
  
    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.name = 'email';
    emailInput.placeholder = 'Email';
    emailInput.classList.add('email-input');
  
    const subscribeButton = document.createElement('button');
    subscribeButton.type = 'submit';
    subscribeButton.textContent = 'Subscribe';
    subscribeButton.classList.add('subscribe-button');
  
    form.appendChild(emailInput);
    form.appendChild(subscribeButton);
  
    this.section.appendChild(heading);
    this.section.appendChild(description);
    this.section.appendChild(form);
  }
  
  StandardProgram.prototype.handleFormSubmit = function(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const emailValue = formData.get('email');
    console.log('Entered email:', emailValue);
  };
  
  StandardProgram.prototype.remove = function() {
    if (this.section.parentNode) {
      this.section.parentNode.removeChild(this.section);
    }
  };
  
  function AdvancedProgram() {
    this.section = document.createElement('div');
    this.section.id = 'joinProgram';
    this.section.classList.add('advanced-program');
  
    const heading = document.createElement('h2');
    heading.textContent = 'Join Our Advanced Program';
  
    const description = document.createElement('p');
    description.textContent = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
  
    const form = document.createElement('form');
    form.addEventListener('submit', this.handleFormSubmit.bind(this));
  
    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.name = 'email';
    emailInput.placeholder = 'Email';
    emailInput.classList.add('email-input');
  
    const subscribeButton = document.createElement('button');
    subscribeButton.type = 'submit';
    subscribeButton.textContent = 'Subscribe to Advanced Program';
    subscribeButton.classList.add('subscribe-button');
    subscribeButton.classList.add('subscribe-button-advanced');
  
    form.appendChild(emailInput);
    form.appendChild(subscribeButton);
  
    this.section.appendChild(heading);
    this.section.appendChild(description);
    this.section.appendChild(form);
  }
  
  AdvancedProgram.prototype.handleFormSubmit = function(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const emailValue = formData.get('email');
    console.log('Entered email for advanced program:', emailValue);
  };
  
  AdvancedProgram.prototype.remove = function() {
    if (this.section.parentNode) {
      this.section.parentNode.removeChild(this.section);
    }
  };
  
 export default function addJoinOurProgramSection(type) {
    const existingContent = document.getElementById('contentToAdd');
 
    const programSection = SectionCreator.create(type);
 
    existingContent.insertAdjacentElement('afterend', programSection.section);
  }

 
  